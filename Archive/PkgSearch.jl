"A module to search the Julia package database."
module PkgSearch

using Requests
# Periodically, search the Julia package directory.

# Get all the github readmes and put them in a... Dict?
# Find a list of every Julia Package
# Create array of strings of all readme urls.
# try to fetch each one.
function getallpkgurls()
    urls = Array{AbstractString,1}()
    for p in Pkg.available()
       fn = joinpath(Pkg.dir("METADATA"),p,"url")
       data = ""
       open(fn) do f
           data = chomp(readlines(f)[1])
       end
       # swap git:: for https::
       gitmatch = r"git"
       m = match(gitmatch, data)
       if m.offset == 1
           data = "https"*data[4:end]
       end
       # chomp the .git off the end
       m = match(gitmatch, data, length(data)-5)
       if m != Void
           data = data[1:m.offset-2]
       end
       push!(urls, joinpath(data,"blob/master/README.md"))
    end
    urls
end



# A function to search for a string.









end # module
