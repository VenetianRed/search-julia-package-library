# Run these things to get a dictionary of (name, URL) pairs
include("IndexPkgReadmes.jl")
using IndexPkgReadmes
urlDict = IndexPkgReadmes.getPkgURLs()

myauth = GitHub.authenticate(ENV["GITHUB_AUTH"]) # get an auth token from the environment

emptyDict = Dict{AbstractString, Int}()
wordCountDict = reduce(
  (dict, pkgName) -> IndexPkgReadmes.processOnePackage( dict, pkgName, urlDict[pkgName], myauth),
  emptyDict, keys(urlDict)
)

writecsv("wordCounts.csv", wordCountDict)
