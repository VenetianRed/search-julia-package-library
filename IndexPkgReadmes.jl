"A module to index the README.md for all packages listed on the Julia package page."
module IndexPkgReadmes

using Gumbo     # Parse HTML
using GitHub    # Fetch README files
using Requests  # Do HTTP GET, POST and the like
import Requests: get

# Scrape the page that lists packages and extract
# URLs for the relevant GitHub repositories
function getPkgURLs()
  # The result, initially empty
  pkgNameToUrlDict = Dict() ;

  # First, get the HTML for the page that lists packages
  JuliaPkgLibURL = "http://pkg.julialang.org"
  pkgListHTML = get( JuliaPkgLibURL )
  statusCode = statuscode( pkgListHTML)
  if( statusCode != 200 )
    warn( "Bork! get($JuliaPkgLibURL) returned with status code $statusCode" )
  else
    # Got the page, now parse that puppy, looking for GitHub URLs
    doc = parsehtml( readall(pkgListHTML) )
    for elem in breadthfirst(doc.root)
      # The things we want are contained in <div>'s with the
      # helpful and informative class attribute "pkgnamedesc".
      if( !(typeof(elem) <: HTMLText) # text elements don't have tags
          && (tag(elem) == Symbol("div"))
          && (getattr(elem, "class") == "pkgnamedesc") )
        # Run through the subtree below the <div> tag
        for subElem in breadthfirst(elem)
          if( !(typeof(subElem) <: HTMLText) && (tag(subElem) == Symbol("a")) )
            pkgNameToUrlDict[getattr(subElem, "name")] = getattr(subElem, "href")
          end
        end
      end
    end
  end

  pkgNameToUrlDict
end

# This is the very heart of the thing: given a chunk of text,
# strip out punctuation, make everything lowercase and then
# count the words by incrementing totals in a dictionary.
function countWords( countDict, text, pkgName )
  # Make everything lowercase and try to deal with hyphenation,
  # then wipe out punctuation and numbers and split into words.
  standardisedText = replace(lowercase(text), r"\-\s*[\n\r]+"," ")
  standardisedText = replace(standardisedText, r"[\d[:punct:]]+"," ")
  wordList = split( standardisedText )
  # println( "$pkgName: found ", length(wordList),  " words" )

  # Iterate through the list of word and update counts accordingly
  for word in wordList
    if( haskey(countDict, word) )
      countDict[word] += 1
    else
      countDict[word] = 1
    end
  end

  # Return the modified dictionary
  countDict
end

# Check whether the file is more than a week old
function cachedFileIsStale( modtime )
  dayInMilliSecs = 1000 * 3600 * 24
  ageInMilliSecs = Float64(now() - Dates.unix2datetime(modtime))

  (ageInMilliSecs / dayInMilliSecs) >= 7.0
end

# Given the name of a package, fetch the text of its
# README file and count the words.
function processOnePackage( countDict, pkgName, pkgURL, authToken )
  # Attempt to get the text of the README file, which may be cached.
  cacheFilePath = "Cache/$pkgName" * "_README.md"
  cacheFileModtime = mtime( cacheFilePath )
  if( (cacheFileModtime == 0.0) || cachedFileIsStale( cacheFileModtime ) )
    # Fetch the README file from GitHub
    try
      # Try to fetch the README file afresh
      nameForSearch = replace( pkgURL, r"http[s]*://github.com/", "" )
      readmeObj = GitHub.readme( nameForSearch, auth = authToken )
      readmeText = String( base64decode( get(readmeObj.content) ) )

      # Write that puppy to the cache and then count words
      write( cacheFilePath, readmeText )
      countWords( countDict, readmeText, pkgName )
    catch recentErr
      if isa( recentErr, ErrorException)
        warn( "$pkgName: appears not to have a README" )
      elseif isa( recentErr, SystemError )
        warn( "$pkgName: couldn't cache the README." )
      end
    end
  else # The file is cached and recent enough
    try
      # slurp the text and count words
      fh = open( cacheFilePath )
      readmeText = readstring( fh )
      countWords( countDict, readmeText, pkgName )
    catch recentErr
      if isa( recentErr, SystemError )
        warn( "$pkgName: couldn't open the cached README." )
      end
    end
  end

  countDict
end

end # module
