# README #

I got the idea for this package at a [Julia workshop](http://www.maths.manchester.ac.uk/~siam/julia16/) organised by the University of Manchester's student chapter of SIAM. I was looking for packages to use while writing my first Julia code and found myself wishing for something analogous to the great search tools one finds at [CPAN](http://search.cpan.org) and so wrote a small program to 

* screen-scrape the list of packages at (http://pkg.julialang.org)
* Use Julia's [GitHub](https://github.com/JuliaWeb/GitHub.jl) package to fetch all the associated README files
* Run through them and build a table that records the total number of appearances of words.

Andy Gibb, from the BBC's research department, was encouraging and cheerfully answered my many naive questions.  About a week later he emailed me about the lovely tool available at <http://genieframework.com/packages>, so I abandoned the project, but if I were to carry on I'd index my cached files, preparing a large sparse matrix whose rows are labelled by packages and whose columns are labelled by words. The *i, j* entry gives the number of times that word *j* appears in package.